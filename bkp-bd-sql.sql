-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           5.6.17 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para portfoliocarlos
DROP DATABASE IF EXISTS `portfoliocarlos`;
CREATE DATABASE IF NOT EXISTS `portfoliocarlos` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `portfoliocarlos`;


-- Copiando estrutura para tabela portfoliocarlos.banner
DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner` (
  `bann_img` varchar(60) NOT NULL,
  `bann_titulo` varchar(50) DEFAULT NULL,
  `bann_chamada` varchar(200) DEFAULT NULL,
  `bann_subchamada` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela portfoliocarlos.banner: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
REPLACE INTO `banner` (`bann_img`, `bann_titulo`, `bann_chamada`, `bann_subchamada`) VALUES
	('banner.jpg', 'Banner 1', 'Faça o site ou app em que tanto sonha', 'Eu posso ajudar-lhe a tornar seu projeto em realidade!');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;


-- Copiando estrutura para tabela portfoliocarlos.configuracoes
DROP TABLE IF EXISTS `configuracoes`;
CREATE TABLE IF NOT EXISTS `configuracoes` (
  `conf_email` varchar(50) DEFAULT NULL,
  `conf_celular` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela portfoliocarlos.configuracoes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `configuracoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `configuracoes` ENABLE KEYS */;


-- Copiando estrutura para tabela portfoliocarlos.contato
DROP TABLE IF EXISTS `contato`;
CREATE TABLE IF NOT EXISTS `contato` (
  `cont_chamada` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela portfoliocarlos.contato: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
REPLACE INTO `contato` (`cont_chamada`) VALUES
	('Deseja entrar em contato comigo? Então, preencha o formulário abaixo que responderei seu e-mail o mais rápido possível!');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;


-- Copiando estrutura para tabela portfoliocarlos.habilidades
DROP TABLE IF EXISTS `habilidades`;
CREATE TABLE IF NOT EXISTS `habilidades` (
  `habi_img` varchar(100) DEFAULT NULL,
  `habi_titulo` varchar(100) DEFAULT NULL,
  `habi_descricao` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela portfoliocarlos.habilidades: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `habilidades` DISABLE KEYS */;
REPLACE INTO `habilidades` (`habi_img`, `habi_titulo`, `habi_descricao`) VALUES
	('angularjs.png', 'Angualr JS', 'Framework baseado em MVC, na escrito Linguagem Javascript. Muito utilizado atualmente no desenvolvimento de sites e aplicações WEB, em conjunto com a Plataforma Node.js(Também escrita em Javascript).');
/*!40000 ALTER TABLE `habilidades` ENABLE KEYS */;


-- Copiando estrutura para tabela portfoliocarlos.sobre_mim
DROP TABLE IF EXISTS `sobre_mim`;
CREATE TABLE IF NOT EXISTS `sobre_mim` (
  `sobr_texto` text,
  `sobr_img` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela portfoliocarlos.sobre_mim: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `sobre_mim` DISABLE KEYS */;
REPLACE INTO `sobre_mim` (`sobr_texto`, `sobr_img`) VALUES
	('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur eius culpa in vel, sapiente recusandae ex itaque illo ipsa! Dolores harum rem magni dolor esse animi officiis, voluptates quisquam quam!', 'carlos-portfolio.jpg');
/*!40000 ALTER TABLE `sobre_mim` ENABLE KEYS */;


-- Copiando estrutura para tabela portfoliocarlos.trabalhos_realizados
DROP TABLE IF EXISTS `trabalhos_realizados`;
CREATE TABLE IF NOT EXISTS `trabalhos_realizados` (
  `trab_titulo` varchar(100) DEFAULT NULL,
  `trab_descricao` text,
  `trab_img` varchar(100) DEFAULT NULL,
  `trab_link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela portfoliocarlos.trabalhos_realizados: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `trabalhos_realizados` DISABLE KEYS */;
REPLACE INTO `trabalhos_realizados` (`trab_titulo`, `trab_descricao`, `trab_img`, `trab_link`) VALUES
	('Plataformanet', 'Site feito para a empresa na qual trabalho atualmente. Site responsivo, utilizando o Framework javascript: AngularJS.', 'plataformanet-desktop.png', 'http://plataformanet.com.br');
/*!40000 ALTER TABLE `trabalhos_realizados` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
