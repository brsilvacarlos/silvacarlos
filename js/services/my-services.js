angular.module('servicos', ['ngResource'])

/**
 * Recurso genérico, que carrega o conteúdo do site
 */
.factory('recursoSite', function($resource, $rootScope){
	return $resource($rootScope.nodeUrl+'/:recurso', {
		query: {
			method: 'get'
		}
	});
})

/**
 * Recurso que envia e-mails
 */
.factory('recursoEnviaEmail', function($resource, $rootScope){
	return $resource($rootScope.nodeMailer + '/scms/enviaContato', null, {
		update : {
			method: 'POST'
		}
	});
});