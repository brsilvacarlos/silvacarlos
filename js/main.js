var app = angular.module('main',['ngRoute','ngAnimate','ngResource','servicos','slickCarousel','ngTouch','ngSanitize', 'angularSpinner']);

	app	.config(function($routeProvider,$locationProvider,$httpProvider, usSpinnerConfigProvider){

		usSpinnerConfigProvider.setDefaults({color: '#36b9eb'});
		
    		usSpinnerConfigProvider.setTheme('temaPreto', {color: 'black', radius:30, width:8, length: 50, scale: .5});
		usSpinnerConfigProvider.setTheme('temaAzul', {color: '#36b9eb', radius:30, width:8, length: 50, scale: .5} );
    		usSpinnerConfigProvider.setTheme('temaPretoPeq', {color: 'black', radius:20, width:6, length: 30, scale: .5});
		usSpinnerConfigProvider.setTheme('temaAzulPeq', {color: '#36b9eb', radius:20, width:6, length: 30, scale: .5} );
		
		$locationProvider.html5Mode({
			enabled:true,
			requireBase:false
		});

		$routeProvider.when('/',{controller:'GeralController'});

	});