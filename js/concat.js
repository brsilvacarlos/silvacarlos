var app = angular.module('main',['ngRoute','ngAnimate','ngResource','servicos','slickCarousel','ngTouch','ngSanitize', 'angularSpinner']);

	app	.config(function($routeProvider,$locationProvider,$httpProvider, usSpinnerConfigProvider){

		usSpinnerConfigProvider.setDefaults({color: '#36b9eb'});
		
    		usSpinnerConfigProvider.setTheme('temaPreto', {color: 'black', radius:30, width:8, length: 50, scale: .5});
		usSpinnerConfigProvider.setTheme('temaAzul', {color: '#36b9eb', radius:30, width:8, length: 50, scale: .5} );
    		usSpinnerConfigProvider.setTheme('temaPretoPeq', {color: 'black', radius:20, width:6, length: 30, scale: .5});
		usSpinnerConfigProvider.setTheme('temaAzulPeq', {color: '#36b9eb', radius:20, width:6, length: 30, scale: .5} );
		
		$locationProvider.html5Mode({
			enabled:true,
			requireBase:false
		});

		$routeProvider.when('/',{controller:'GeralController'});

	});
angular.module('main').controller('GeralController', function($scope, $rootScope, $location, anchorSmoothScroll){

	// $rootScope.nodeUrl = 'http://localhost:3000';
	// $rootScope.nodeUrl = 'http://silvacarlos.herokuapp.com';
	$rootScope.nodeUrl = 'https://silvacarlos.herokuapp.com';

	// $rootScope.nodeMailer = 'http://localhost:3000';
	$rootScope.nodeMailer = 'https://silvacarlosmailsender.herokuapp.com';

	$rootScope.direcTemplUrl = './js/directives/templates/';


	 $rootScope.gotoElement = function (eID){
		 anchorSmoothScroll.scrollTo(eID);
	};

	$rootScope.slickConfig = {
		enabled: true,
		autoplay: true,
		dots: true,
		draggable: true,
		lazyLoad: 'ondemand',
		pauseOnFocus: true,
		pauseOnHover: true,
		focusOnSelect: true,
		// fade: true,
		/*arrows: false, */
		autoplaySpeed: 4000
 	}


 	// DEFINE A QUANTIDADE DE SLIDES, PELO TAMANHO DA TELA
 	if(window.innerWidth <=768){
		//celulares
		$rootScope.slideHabilidades = 2;
		$rootScope.slideTrabalhos = 1;
		$rootScope.nSlides = 1;
		$rootScope.arrows = false;
	} else if(window.innerWidth <=992){
		//tablets
		$rootScope.slideHabilidades = 3;
		$rootScope.slideTrabalhos = 2;
		$rootScope.nSlides = 2;
		$rootScope.arrows = false;
	} else{
		// monitores
		$rootScope.slideHabilidades = 5;
		$rootScope.slideTrabalhos = 3;
		$rootScope.nSlides = 2;
		$rootScope.arrows = true;
	}

});
angular.module('main').controller('BannerController', function($scope, $rootScope, recursoSite){

	// spinner
	$scope.loading = true;

	recursoSite.query({recurso: 'banner'}, 
		function(data){
			// spinner
			$scope.loading = false;
			
			$scope.banner = data[0];
			// console.log(data);
		}, 
		function(error){
			console.log(data);
	});

});
angular.module('main').controller('SobreMimController', function($scope, $rootScope, recursoSite){

	// spinner
	$scope.loading = true;	

	recursoSite.query({recurso: 'sobre_mim'}, function(data){
		// spinner
		$scope.loading = false;

		$scope.sobreMim = data[0];

		// $scope.sobreMim.sobr_texto = $sce.trustAsHtml(data[0].sobr_texto);
	
	}, function(error){
		console.log(error);
	});

});
angular.module('main').controller('HabilidadesControler', function($scope, $rootScope, recursoSite){

	$scope.carregaSlick = false;

	// spinner
	$scope.loading = true;

	recursoSite.query({recurso: 'habilidades'}, function(data){
		// spinner
		$scope.loading = false;

		// REMOVE OS ITENS INATIVOS DO ARRAY
		for (var i = 0; i < data.length; i++) {
			if (data[i].habi_ativo == 0) {
				data.splice(i, 1);
			}

			// EDITA A URL PARA TRAZER A IMAGEM TRANSFORMADA DO CLOUDNARY
			data[i].habi_img = data[i].habi_img.replace('image/upload/', 'image/upload/c_scale,q_auto:eco,w_130/');
		}

		$scope.habilidades = data;
		$scope.carregaSlick = true;
		// console.log(data);
	}, function(error){
		console.log(error);
	});

	/*$scope.slickConfig = {
	        enabled: true
	}*/


});
angular.module('main').controller('TrabalhosRealizados', function($scope, $rootScope, recursoSite, $window){

	// spinner
	$scope.loading = true;

	recursoSite.query({recurso: 'trabalhos_realizados'}, function(data){
		// spinner
		$scope.loading = false;

		for (var i = 0; i < data.length; i++) {
			// REMOVE OS ITENS INATIVOS DO ARRAY
			if (data[i].trab_ativo == 0) {
				data.splice(i, 1);
			}

			// EDITA A URL PARA TRAZER A IMAGEM TRANSFORMADA DO CLOUDNARY
			data[i].trab_img = data[i].trab_img.replace('image/upload/', 'image/upload/c_scale,q_auto:eco,w_300/');
		}

		$scope.trabalhos = data;


		$scope.carregaSlick = true;
		// console.log(data);
	},function(error){
		console.log(error);
	})

});
angular.module('main').controller('ContatoController', function($scope, $rootScope, recursoSite, recursoEnviaEmail, $timeout, $sce){

	/*// spinner
	$scope.loading = true;
	recursoSite.query({recurso: 'contato'}, function(data){
		// spinner
		$scope.loading = false;

		$scope.contatoNode = data[0];

	}, function(error){
		console.log(error);
	});*/

	//envia formulario de contato
	$scope.enviarContato = function(){
		//se formulario estiver valido
		if($scope.formContato.$valid){
			$scope.alert = "Enviando mensagem por favor aguarde!";
			$scope.tipoalert = "info";
			//console.log($scope.mensagem);
			
			/**
			 * Envia o e-mail com os dados informados
			 */
			recursoEnviaEmail.save($scope.contato, function(data){
				$scope.alert = "Mensagem enviada com sucesso!";
				$scope.tipoalert = "success";
				$scope.contato = {};
				$timeout(function() {
				     $scope.alert = "";
					$scope.tipoalert = "";
				}, 3000); //tempo em milissegundos do delay. Nesse caso, são 3 segundos.
			}, function(erro){
				console.log(erro);
				$scope.tipoalert = "danger";
				$scope.alert = "Erro ao enviar mensagem. Tente novamente mais tarde";
			});

		} //se o formulário nao for valido...
		else{
			$scope.tipoalert = "danger";
			$scope.alert = "Preencha corretamente todos os campos para poder enviar a mensagem!";
		}
	}


})
angular.module('main')
.directive('underline', function($rootScope){
	var ddo = {};

	ddo.restrict = 'E';
	// ddo.templateUrl = $rootScope.direcTemplUrl + 'underline.html';
	ddo.templateUrl = 'js/directives/templates/underline.html';

	return ddo;
});

angular.module('main')
.directive('topo', function($rootScope){
	var ddo = {};

	ddo.restrict = 'E';
	ddo.templateUrl = 'js/directives/templates/topo.html';

	return ddo;
});

angular.module('main')
.directive('sobreMim', function($rootScope){
	var ddo = {};

	ddo.restrict = 'E';
	ddo.templateUrl = 'js/directives/templates/sobre-mim.html';

	return ddo;
});

angular.module('main')
.directive('habilidades', function($rootScope){
	var ddo = {};

	ddo.restrict = 'E';
	ddo.templateUrl = 'js/directives/templates/habilidades.html';

	return ddo;
});

angular.module('main')
.directive('trabalhosRealizados', function($rootScope){
	var ddo = {};

	ddo.restrict = 'E';
	ddo.templateUrl = 'js/directives/templates/trabalhos-realizados.html';

	return ddo;
});

angular.module('main')
.directive('contato', function($rootScope){
	var ddo = {};

	ddo.restrict = 'E';
	ddo.templateUrl = 'js/directives/templates/contato.html';

	return ddo;
});

angular.module('main').service('anchorSmoothScroll', function(){
    
    this.scrollTo = function(eID) {

        // This scrolling function 
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
        
        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for ( var i=startY; i<stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( var i=startY; i>stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
        
        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }
        
        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }

    };
    
});
angular.module('servicos', ['ngResource'])

/**
 * Recurso genérico, que carrega o conteúdo do site
 */
.factory('recursoSite', function($resource, $rootScope){
	return $resource($rootScope.nodeUrl+'/:recurso', {
		query: {
			method: 'get'
		}
	});
})

/**
 * Recurso que envia e-mails
 */
.factory('recursoEnviaEmail', function($resource, $rootScope){
	return $resource($rootScope.nodeMailer + '/scms/enviaContato', null, {
		update : {
			method: 'POST'
		}
	});
});