angular.module('main').controller('GeralController', function($scope, $rootScope, $location, anchorSmoothScroll){

	// $rootScope.nodeUrl = 'http://localhost:3000';
	// $rootScope.nodeUrl = 'http://silvacarlos.herokuapp.com';
	$rootScope.nodeUrl = 'https://silvacarlos.herokuapp.com';

	// $rootScope.nodeMailer = 'http://localhost:3000';
	$rootScope.nodeMailer = 'https://silvacarlosmailsender.herokuapp.com';

	$rootScope.direcTemplUrl = './js/directives/templates/';


	 $rootScope.gotoElement = function (eID){
		 anchorSmoothScroll.scrollTo(eID);
	};

	$rootScope.slickConfig = {
		enabled: true,
		autoplay: true,
		dots: true,
		draggable: true,
		lazyLoad: 'ondemand',
		pauseOnFocus: true,
		pauseOnHover: true,
		focusOnSelect: true,
		// fade: true,
		/*arrows: false, */
		autoplaySpeed: 4000
 	}


 	// DEFINE A QUANTIDADE DE SLIDES, PELO TAMANHO DA TELA
 	if(window.innerWidth <=768){
		//celulares
		$rootScope.slideHabilidades = 2;
		$rootScope.slideTrabalhos = 1;
		$rootScope.nSlides = 1;
		$rootScope.arrows = false;
	} else if(window.innerWidth <=992){
		//tablets
		$rootScope.slideHabilidades = 3;
		$rootScope.slideTrabalhos = 2;
		$rootScope.nSlides = 2;
		$rootScope.arrows = false;
	} else{
		// monitores
		$rootScope.slideHabilidades = 5;
		$rootScope.slideTrabalhos = 3;
		$rootScope.nSlides = 2;
		$rootScope.arrows = true;
	}

});