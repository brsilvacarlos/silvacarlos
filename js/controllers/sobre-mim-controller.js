angular.module('main').controller('SobreMimController', function($scope, $rootScope, recursoSite){

	// spinner
	$scope.loading = true;	

	recursoSite.query({recurso: 'sobre_mim'}, function(data){
		// spinner
		$scope.loading = false;

		$scope.sobreMim = data[0];

		// $scope.sobreMim.sobr_texto = $sce.trustAsHtml(data[0].sobr_texto);
	
	}, function(error){
		console.log(error);
	});

});