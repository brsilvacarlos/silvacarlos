angular.module('main').controller('HabilidadesControler', function($scope, $rootScope, recursoSite){

	$scope.carregaSlick = false;

	// spinner
	$scope.loading = true;

	recursoSite.query({recurso: 'habilidades'}, function(data){
		// spinner
		$scope.loading = false;

		// REMOVE OS ITENS INATIVOS DO ARRAY
		for (var i = 0; i < data.length; i++) {
			if (data[i].habi_ativo == 0) {
				data.splice(i, 1);
			}

			// EDITA A URL PARA TRAZER A IMAGEM TRANSFORMADA DO CLOUDNARY
			data[i].habi_img = data[i].habi_img.replace('image/upload/', 'image/upload/c_scale,q_auto:eco,w_130/');
		}

		$scope.habilidades = data;
		$scope.carregaSlick = true;
		// console.log(data);
	}, function(error){
		console.log(error);
	});

	/*$scope.slickConfig = {
	        enabled: true
	}*/


});