angular.module('main').controller('ContatoController', function($scope, $rootScope, recursoSite, recursoEnviaEmail, $timeout, $sce){

	/*// spinner
	$scope.loading = true;
	recursoSite.query({recurso: 'contato'}, function(data){
		// spinner
		$scope.loading = false;

		$scope.contatoNode = data[0];

	}, function(error){
		console.log(error);
	});*/

	//envia formulario de contato
	$scope.enviarContato = function(){
		//se formulario estiver valido
		if($scope.formContato.$valid){
			$scope.alert = "Enviando mensagem por favor aguarde!";
			$scope.tipoalert = "info";
			//console.log($scope.mensagem);
			
			/**
			 * Envia o e-mail com os dados informados
			 */
			recursoEnviaEmail.save($scope.contato, function(data){
				$scope.alert = "Mensagem enviada com sucesso!";
				$scope.tipoalert = "success";
				$scope.contato = {};
				$timeout(function() {
				     $scope.alert = "";
					$scope.tipoalert = "";
				}, 3000); //tempo em milissegundos do delay. Nesse caso, são 3 segundos.
			}, function(erro){
				console.log(erro);
				$scope.tipoalert = "danger";
				$scope.alert = "Erro ao enviar mensagem. Tente novamente mais tarde";
			});

		} //se o formulário nao for valido...
		else{
			$scope.tipoalert = "danger";
			$scope.alert = "Preencha corretamente todos os campos para poder enviar a mensagem!";
		}
	}


})