angular.module('main').controller('TrabalhosRealizados', function($scope, $rootScope, recursoSite, $window){

	// spinner
	$scope.loading = true;

	recursoSite.query({recurso: 'trabalhos_realizados'}, function(data){
		// spinner
		$scope.loading = false;

		for (var i = 0; i < data.length; i++) {
			// REMOVE OS ITENS INATIVOS DO ARRAY
			if (data[i].trab_ativo == 0) {
				data.splice(i, 1);
			}

			// EDITA A URL PARA TRAZER A IMAGEM TRANSFORMADA DO CLOUDNARY
			data[i].trab_img = data[i].trab_img.replace('image/upload/', 'image/upload/c_scale,q_auto:eco,w_300/');
		}

		$scope.trabalhos = data;


		$scope.carregaSlick = true;
		// console.log(data);
	},function(error){
		console.log(error);
	})

});