angular.module('main')
.directive('trabalhosRealizados', function($rootScope){
	var ddo = {};

	ddo.restrict = 'E';
	ddo.templateUrl = 'js/directives/templates/trabalhos-realizados.html';

	return ddo;
});
