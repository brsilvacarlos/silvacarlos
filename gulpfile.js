var gulp = require('gulp');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var htmlmin = require('gulp-htmlmin');
var strip = require('gulp-strip-comments');
 
gulp.task('concat-css', function() {
	return gulp.src([
		'node_modules/bootstrap/dist/css/bootstrap.min.css',
		'css/font-face.css',
		'css/style.css',
		'node_modules/slick-carousel/slick/slick.css',
		'node_modules/slick-carousel/slick/slick-theme.css'
		])

    		.pipe(concat('concat.css'))
    		.pipe(gulp.dest('./css/'));
});

 
gulp.task('cssminify', function () {
    gulp.src('css/concat.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('css'));
});

gulp.task('angular-core-js', function() {
  	return gulp.src([
	'node_modules/angular/angular.min.js',
	'node_modules/angular-animate/angular-animate.min.js',
	'node_modules/angular-resource/angular-resource.min.js',
	'node_modules/angular-route/angular-route.min.js',
	'node_modules/angular-touch/angular-touch.min.js',
	'node_modules/angular-sanitize/angular-sanitize.min.js'
  		])
    		.pipe(concat('angular-core.js'))
    		.pipe(gulp.dest('./js/'));
});

gulp.task('concat-main-js', function() {
  	return gulp.src([
	'js/main.js',
	'js/controllers/geral-controller.js',
	'js/controllers/banner-controller.js',
	'js/controllers/sobre-mim-controller.js',
	'js/controllers/habilidades-controller.js',
	'js/controllers/trabalhos-realizados-controller.js',
	'js/controllers/contato-controller.js',
	'js/directives/underline.js',
	'js/directives/topo.js',
	'js/directives/sobre-mim.js',
	'js/directives/habilidades.js',
	'js/directives/trabalhos-realizados.js',
	'js/directives/contato.js',
	'js/services/anchor-smooth-scroll.js',
	'js/services/my-services.js'
  		])
    		.pipe(concat('concat.js'))
    		.pipe(gulp.dest('./js/'));
});

gulp.task('libs-js', function() {
  	return gulp.src([
	'node_modules/slick-carousel/slick/slick.min.js',
	'node_modules/angular-slick-carousel/dist/angular-slick.min.js',
	'node_modules/angular-spinner/dist/angular-spinner.min.js'
  		])
    		.pipe(concat('concat-libs.js'))
    		.pipe(gulp.dest('./js/'));
});

gulp.task('strip-comments', function () {
  return gulp.src('html-original/index.html')
    .pipe(strip())
    .pipe(gulp.dest('html-original/no-comments/'));
});
 

gulp.task('html-minify', function() {
  return gulp.src('html-original/no-comments/index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('.'));
});



gulp.task('default', [ 'concat-css', 'cssminify', 'angular-core-js', 'concat-main-js', 'libs-js', 'strip-comments', 'html-minify' ]);